//
//  GHUserCell.swift
//  GHSimpleClient
//
//  Created by Sasha Evtushenko on 4/4/19.
//  Copyright © 2019 Sasha Evtushenko. All rights reserved.
//

import UIKit

class GHUserCell: UITableViewCell {

    // Outlets
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var userLogin: UILabel!
    @IBOutlet weak var userID: UILabel!
    
}
