//
//  GHUser.swift
//  GHSimpleClient
//
//  Created by Sasha Evtushenko on 4/4/19.
//  Copyright © 2019 Sasha Evtushenko. All rights reserved.
//

import Foundation

struct GHUser {
    
    var avatar_url: String
    var login: String
    var id: Int

    init(ghUserAvatar: String, ghUserLogin: String, ghUserID: Int) {
        avatar_url = ghUserAvatar
        login = ghUserLogin
        id = ghUserID
    }
}
