//
//  Constants.swift
//  GHSimpleClient
//
//  Created by Sasha Evtushenko on 4/4/19.
//  Copyright © 2019 Sasha Evtushenko. All rights reserved.
//

import Foundation

let ALL_USERS_URL = "https://api.github.com/users?since="
// ?page=1&per_page=30
let SELECTED_USER_URL = "https://api.github.com/users/"
