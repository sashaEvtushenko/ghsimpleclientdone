//
//  ViewController.swift
//  GHSimpleClient
//
//  Created by Sasha Evtushenko on 4/4/19.
//  Copyright © 2019 Sasha Evtushenko. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import SDWebImage

class UsersVC: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    var allUsers = [GHUser]()
    var selectedUserLogin: String = ""
    var refreshControl = UIRefreshControl()
    var loadMore = false
    
    @IBOutlet weak var allUsersTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        allUsersTableView.delegate = self
        allUsersTableView.dataSource = self
        refreshControl.addTarget(self, action: #selector(refresh(sender:)), for: .valueChanged)
        allUsersTableView.addSubview(refreshControl)
        
        getAllUsers()
        
    }
    
    @objc func refresh(sender: AnyObject) {
        getAllUsers()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return allUsers.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = allUsersTableView.dequeueReusableCell(withIdentifier: "userCell") as! GHUserCell
        cell.userLogin.text = allUsers[indexPath.row].login
        cell.userID.text = String(allUsers[indexPath.row].id)
        cell.userImage.sd_setImage(with: URL(string: allUsers[indexPath.row].avatar_url))
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedUserLogin = allUsers[indexPath.row].login
        performSegue(withIdentifier: "showUserDetails", sender: nil)
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showUserDetails" {
            if let detailsVC = segue.destination as? UserDetailsVC {
                detailsVC.selectedUser = selectedUserLogin
            }
        }
    }
    
    func getAllUsers() {
        
        Alamofire.request(ALL_USERS_URL, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: nil).responseJSON { (response) in
            
            if response.result.isSuccess {
                let usersJSON = JSON(response.result.value!)
                for index in 0...usersJSON.count - 1 {
                    let login = usersJSON[index]["login"].stringValue
                    let id = usersJSON[index]["id"].intValue
                    let avatar_url = usersJSON[index]["avatar_url"].stringValue
                    self.allUsers.append(GHUser(ghUserAvatar: avatar_url, ghUserLogin: login, ghUserID: id))
                }
                self.allUsersTableView.reloadData()
                self.refreshControl.endRefreshing()
            }
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let offsetY = scrollView.contentOffset.y
        let contentHeight = scrollView.contentSize.height
        
        if offsetY > contentHeight - scrollView.frame.height {
            if !loadMore {
                loadMoreUsers()
            }
        }
    }
    
    func loadMoreUsers() {
        loadMore = true
        
        Alamofire.request(ALL_USERS_URL + "\(allUsers[allUsers.count - 1].id)", method: .get, parameters: nil, encoding: JSONEncoding.default, headers: nil).responseJSON { (response) in
            
            if response.result.isSuccess {
                let usersJSON = JSON(response.result.value!)
                for index in 0...usersJSON.count - 1 {
                    let login = usersJSON[index]["login"].stringValue
                    let id = usersJSON[index]["id"].intValue
                    let avatar_url = usersJSON[index]["avatar_url"].stringValue
                    self.allUsers.append(GHUser(ghUserAvatar: avatar_url, ghUserLogin: login, ghUserID: id))
                }
                self.allUsersTableView.reloadData()
                self.loadMore = false
            }
        }
    }
}

