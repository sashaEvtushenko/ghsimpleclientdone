//
//  UserDetailsVC.swift
//  GHSimpleClient
//
//  Created by Sasha Evtushenko on 4/4/19.
//  Copyright © 2019 Sasha Evtushenko. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import SDWebImage

class UserDetailsVC: UIViewController {
    
    // Outlets
    @IBOutlet weak var avatarImage: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var orgLabel: UILabel!
    @IBOutlet weak var followingLabel: UILabel!
    @IBOutlet weak var followersLabel: UILabel!
    @IBOutlet weak var sinceLabel: UILabel!
    
    var selectedUser: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        getSelectedUser(user: selectedUser)
    }
    
    @IBAction func backButtonPressed(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    func getSelectedUser(user: String) {
        
        Alamofire.request(SELECTED_USER_URL + user, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: nil).responseJSON { (response) in
            
            if response.result.isSuccess {
                let userJSON = JSON(response.result.value!)
                self.updateUI(json: userJSON)
            }
        }
    }
    
    func updateUI(json: JSON) {
        nameLabel.text = json["name"].stringValue
        emailLabel.text = json["email"].stringValue
        orgLabel.text = json["company"].stringValue
        followingLabel.text = json["following"].stringValue
        followersLabel.text = json["followers"].stringValue
        sinceLabel.text = json["created_at"].stringValue
        avatarImage.sd_setImage(with: URL(string: json["avatar_url"].stringValue))
    }
}
